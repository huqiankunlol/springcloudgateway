#!/bin/sh

## service name
APP_NAME=gateway
## 启动等待最长时间
MAX_START_WAIT_SEC=60
## 当前等待时间
WAIT_SEC=1
## 启动成功检查的端口条件
START_PORT=9999

SERVICE_DIR=/home/port/$APP_NAME
APP_DIR=$HOME
JAVA_JDK=jdk1.8.0_144
SERVICE_NAME=$APP_NAME
JAR_NAME=$SERVICE_NAME\.jar
PID=$SERVICE_NAME\.pid
PID_BAK=$PID.bak

cd $SERVICE_DIR

case "$1" in

    start)
        nohup java -Djava.security.egd=file:/dev/./urandom -server -Xms1024m -Xmx1024m -Xmn512m -Xss256k -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=256m -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:CMSFullGCsBeforeCompaction=5 -XX:+UseCMSCompactAtFullCollection -Xloggc:gc.log -XX:+PrintGCDetails -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/port/logs/$APP_NAME/ -jar $JAR_NAME >/dev/null 2>&1 &
        echo $! > $SERVICE_DIR/$PID
        echo "================================================== start $SERVICE_NAME begin... please wait $MAX_START_WAIT_SEC sec"
        while((WAIT_SEC < MAX_START_WAIT_SEC))
        do
	    P_ID=`ps -ef | grep -w "$JAR_NAME" | grep -w "java" | grep -v "grep" | awk '{print $2}'`
            P_ID_COUNT=`ps -ef | grep -w "$JAR_NAME" | grep -w "java" | grep -v "grep" | wc -l`
            PORT_COUNT=`netstat -apn | grep "$START_PORT" | grep "java" | grep "LISTEN" | wc -l`
            if [ $P_ID_COUNT = 0 ]||[ $PORT_COUNT = 0 ];then
                echo "================================================== start $SERVICE_NAME begin... wait time :$WAIT_SEC / $MAX_START_WAIT_SEC | P_ID_COUNT=$P_ID_COUNT, PORT_COUNT=$PORT_COUNT"
		sleep 1
            else
                echo "================================================== start $SERVICE_NAME succeeded. pid=$P_ID, PORT=$START_PORT costTime:$WAIT_SEC"
                let "WAIT_SEC = MAX_START_WAIT_SEC"
            fi
            let "WAIT_SEC += 1"
        done
        ;;

    stop)
		echo "=== stop $SERVICE_NAME begin... please wait $MAX_START_WAIT_SEC sec"
        kill -15 `cat $SERVICE_DIR/$PID`
        mv $SERVICE_DIR/$PID $SERVICE_DIR/$PID_BAK
        rm -rf $SERVICE_DIR/$PID

		while((WAIT_SEC < MAX_START_WAIT_SEC))
        do
			P_ID=`ps -ef | grep -w "$JAR_NAME" | grep -w "java" | grep -v "grep" | awk '{print $2}'`
            P_ID_COUNT=`ps -ef | grep -w "$JAR_NAME" | grep -w "java" | grep -v "grep" | wc -l`
            if [ $P_ID_COUNT != 0 ]; then
                echo "=== stop $SERVICE_NAME begin by (kill -15)... wait time :$WAIT_SEC / $MAX_START_WAIT_SEC | P_ID_COUNT=$P_ID_COUNT"
				sleep 1
            else
                echo "=== stop $SERVICE_NAME succeeded by (kill -15). costTime:$WAIT_SEC"
                let "WAIT_SEC = MAX_START_WAIT_SEC"
            fi
            let "WAIT_SEC += 1"
        done

		P_ID=`ps -ef | grep -w "$JAR_NAME" | grep -w "java" | grep -v "grep" | awk '{print $2}'`
        P_ID_COUNT=`ps -ef | grep -w "$JAR_NAME" | grep -w "java" | grep -v "grep" | wc -l`
		if [ $P_ID_COUNT != 0 ];then
			echo "=== stop $SERVICE_NAME begin by (kill -15) failed. begin kill -9 $SERVICE_NAME process, pid is:$P_ID"
            kill -9 $P_ID
		fi


        ;;

    #restart)
    #    $0 stop
    #    sleep 2
    #    $0 start
    #    ;;

    *)
      echo "invalid arguments:$1"
      ;;

esac
exit 0
