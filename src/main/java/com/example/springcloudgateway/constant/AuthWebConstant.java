package com.example.springcloudgateway.constant;

/**
 * @author yijie
 */
public class AuthWebConstant {
    /**
     * must be: xxx-${bizName}
     */
    final public static String MODULE = "web-auth";
    /**
     * login cache must be: CACHE_WEB_${bizName}.toUpperCase()_LOGIN
     */
    final public static String CACHE_WEB_AUTH_LOGIN = "CACHE_WEB_AUTH_LOGIN";
    final public static String CACHE_WEB_AUTH_KAPTCHA = "CACHE_WEB_AUTH_KAPTCHA";

    final public static String DEFAULT_CONTENT_TYPE = "application/json";
    final public static String HEADER_TICKET = "ticket";
    final public static String HEADER_SIGN ="sign";
    final public static String HEADER_MODULE = "module";
    final public static String PARAM_USERNAME = "username";
    final public static String PARAM_PASSWORD = "password";

    final public static String PREFIX_TICKET = "T";
    final public static String PREFIX_LOGIN_USER = "USER-LOGIN-";
    final public static String PREFIX_DEFAULT_PASSWORD = "PWD-";

    //final public static String URI_PREFIX_LOGIN = "/auth/user/admin";
    final public static String URI_PREFIX_LOGIN = "/auth/login";
    final public static String URI_PREFIX_LOGOUT = "/auth/logout";
    final public static String URI_PREFIX_KAPTCHA = "/auth/kaptcha";
    final public static String URI_PREFIX_SUBCOMPANY = "/auth/subcompany";
    
    final public static String URI_UPLOAD = "/api/pub/download/uploadSn,/api/trm/termstock/export,/api/pub/download/getStreamFile,/api/pub/download/getZipFile,/api/pub/download/downloadSnTemp,/api/activity/policy/qryPolicyById,/api/trans/monPft/export";
//    final public static String URI_UPLOAD = "/api/pub/download/uploadSn,/trm/termstock/export";


    final public static String URI_PREFIX_STOCK = "/trm/termstock";




    final public static String URI_PREFIX_PERMISSION = "/auth/permission";
    final public static String URI_PREFIX_ROLE = "/auth/role";
    final public static String URI_PREFIX_USER = "/auth/user";
    final public static String URI_PREFIX_SALES = "/auth/sales";
    final public static String URI_PREFIX_COMPANY = "/auth/company";
    final public static String URI_PREFIX_MSG = "/pub/msg";

    final public static String TICKET_PASS_USERS = "PASS";

    final public static String URI_PASS_PUB = "";
    final public static String URI_PASS_MEMBER = "";
    final public static String URI_PASS_COMMODITY = "";
    final public static String URI_PASS_ACTIVITY = "";
    final public static String URI_PASS_NOTIFY = "";
    final public static String URI_PASS_FUNC = "/auth/sales/qryList,/auth/company/qryList,/pub/msg/sendCode,/agt/agtmanager/initOpen,/agt/agtmanager/chkOneAgt,/agt/agtmanager/chkOneAgt4Open,/agt/agtmanager/chkNmAndPolicy,/agt/agtmanager/qryProductTypeBySales,/activity/policy/getList,/auth/subcompany/qryList,/activity/policy/qryOrgPolicysBySales,/pub/area/qryProvCityArea";
//    final public static String URI_PASS_NOTIFY = "";
}
