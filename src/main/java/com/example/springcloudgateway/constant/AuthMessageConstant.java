package com.example.springcloudgateway.constant;

/**
 * @author yijie
 */
public class AuthMessageConstant {
    final public static String MSG_LOGIN_SUCCESS = "login.success";
    final public static String MSG_LOGIN_FAILURE = "login.fail";
    final public static String MSG_LOGOUT_SUCCESS = "logout.success";

    final public static String MSG_TICKET_UNKNOW = "ticket.unknow";
    final public static String MSG_TICKET_INVALID = "ticket.illegal";
    final public static String MSG_SIGN_INVALID = "sign.illegal";

    final public static String MSG_KAPTCHA_INVALID = "kaptcha.invalid";
    final public static String MSG_KAPTCHA_TIMEOUT = "kaptcha.timeout";
    final public static String MSG_KAPTCHA_TEXT_EMPTY = "kaptcha.text.empty";
    final public static String MSG_KAPTCHA_TOKEN_EMPTY = "kaptcha.token.empty";

    final public static String MSG_PASSWORD_WRONG_OLD = "password.wrong.old";

    final public static String MSG_USERNAME_ISUSE = "username.isuse";
    final public static String MSG_EMAIL_ISUSE = "email.isuse";
    final public static String MSG_MOBILE_ISUSE= "mobile.isuse";
}
