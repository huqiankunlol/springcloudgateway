package com.example.springcloudgateway.util;

import org.apache.commons.lang3.StringUtils;

import java.nio.charset.Charset;

/**
 * @author yijie
 */
public class CharsetUtil {

    final public static String DEFAULT_CHARSET = "UTF-8";

    public static String getCharset(String charset) {
        return StringUtils.isBlank(charset) ? DEFAULT_CHARSET : charset;
    }

    public static String getSystemCharset() {
        return Charset.defaultCharset().toString();
    }

}
