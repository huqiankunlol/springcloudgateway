package com.example.springcloudgateway.encrypt;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * @author yijie
 */
public class Sha {

    /**
     * sha1
     *
     * @param data original string
     * @return sha1 string
     */
    public static String sha1(final String data) {
        return DigestUtils.sha1Hex(data);
    }

    /**
     * sha256
     *
     * @param data original string
     * @return sha256 string
     */
    public static String sha256(final String data) {
        return DigestUtils.sha256Hex(data);
    }

    /**
     * sha384
     *
     * @param data original string
     * @return sha384 string
     */
    public static String sha384(final String data) {
        return DigestUtils.sha384Hex(data);
    }

    /**
     * sha512
     *
     * @param data original string
     * @return sha512 string
     */
    public static String sha512(final String data) {
        return DigestUtils.sha512Hex(data);
    }

}
