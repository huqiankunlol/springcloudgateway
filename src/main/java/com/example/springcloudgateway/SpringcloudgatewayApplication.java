package com.example.springcloudgateway;

import com.example.springcloudgateway.resolver.UrlResolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


/**
 * @version V1.0
 * @author: hqk
 * @date: 2020/5/12 14:17
 * @Description:  启动类
 */
@SpringBootApplication
public class SpringcloudgatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudgatewayApplication.class, args);
    }


    @Bean
    public UrlResolver urlResolver(){
        return new UrlResolver();
    }


}
